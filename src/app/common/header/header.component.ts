import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, Event as NavigationEvent  } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  path: any = [];

  constructor(private router: Router)
  {
    this.router.events
      .subscribe(
        (event: NavigationEvent) => {
          if(event instanceof NavigationStart) {
            this.path = [];
            let path = event.url;
            let parts = path.split('/');
            for(var i=0; i<parts.length; ++i) {
              if(parts[i].length == 0) {
                continue;
              }
              let full=""
              for(var j=0; j<=i; ++j)
              {
                full += "/";
                full += parts[j];
              }
              var partial = parts[i].split("-").join(" ");
              this.path.push({ full: full, partial: partial });
            }
          }
        });
  }

  ngOnInit(): void {
  }

  onTap(): void {
    console.log('clicked');
    var hidden = localStorage.getItem('ShowHidden') == 'true';
    if(hidden) {
      localStorage.setItem('ShowHidden', 'false');
    }
    else {
      localStorage.setItem('ShowHidden', 'true');
    }
  }

}
