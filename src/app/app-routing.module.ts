import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { GreenDotComponent } from './campaigns/green-dot/green-dot.component';
import { QuamComponent } from './campaigns/quam/quam.component';
import { RiseOfLadyIrulanComponent } from './campaigns/rise-of-lady-irulan/rise-of-lady-irulan.component';
import { HomebrewComponent } from './homebrew/homebrew.component';
import { ItemsComponent } from './homebrew/items/items.component';
import { RacesComponent } from './homebrew/races/races.component';
import { RatfolkComponent } from './homebrew/races/ratfolk/ratfolk.component';
import { SpellsComponent } from './homebrew/spells/spells.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { EtiquetteComponent } from './rules/etiquette/etiquette.component';
import { HomeRulesComponent } from './rules/home-rules/home-rules.component';
import { LevelingComponent } from './rules/leveling/leveling.component';
import { RulesComponent } from './rules/rules.component';
import { ShipCombatComponent } from './rules/ship-combat/ship-combat.component';
import { SpelljammingComponent } from './rules/spelljamming/spelljamming.component';
import { PlanarEmpireComponent } from './setting/planar-empire/planar-empire.component';
import { SettingComponent } from './setting/setting.component';
import { PlayableRacesComponent } from './setting/playable-races/playable-races.component';

const extraOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
  onSameUrlNavigation: 'reload',
  useHash: true,
};

const routes: Routes = [
  {
    path: '',
    component: MainMenuComponent,
  },
  {
    path: 'campaigns',
    component: CampaignsComponent,
  },
  {
    path: 'campaigns/rise-of-lady-irulan',
    component: RiseOfLadyIrulanComponent,
  },
  {
    path: 'campaigns/green-dot',
    component: GreenDotComponent,
  },
  {
    path: 'campaigns/quam',
    component: QuamComponent,
  },
  {
    path: 'rules',
    component: RulesComponent,
  },
  {
    path: 'rules/etiquette',
    component: EtiquetteComponent,
  },
  {
    path: 'rules/home-rules',
    component: HomeRulesComponent,
  },
  {
    path: 'rules/leveling',
    component: LevelingComponent,
  },
  {
    path: 'rules/ship-combat',
    component: ShipCombatComponent,
  },
  {
    path: 'rules/spelljamming',
    component: SpelljammingComponent,
  },
  {
    path: 'homebrew',
    component: HomebrewComponent,
  },
  {
    path: 'homebrew/races',
    component: RacesComponent,
  },
  {
    path: 'homebrew/races/ratfolk',
    component: RatfolkComponent,
  },
  {
    path: 'homebrew/spells',
    component: SpellsComponent,
  },
  {
    path: 'homebrew/items',
    component: ItemsComponent,
  },
  {
    path: 'setting',
    component: SettingComponent,
  },
  {
    path: 'setting/planar-empire',
    component: PlanarEmpireComponent,
  },
  {
    path: 'setting/races',
    component: PlayableRacesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, extraOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
