import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanarEmpireComponent } from './planar-empire.component';

describe('PlanarEmpireComponent', () => {
  let component: PlanarEmpireComponent;
  let fixture: ComponentFixture<PlanarEmpireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanarEmpireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanarEmpireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
