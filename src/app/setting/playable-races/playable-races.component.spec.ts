import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayableRacesComponent } from './playable-races.component';

describe('PlayableRacesComponent', () => {
  let component: PlayableRacesComponent;
  let fixture: ComponentFixture<PlayableRacesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayableRacesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayableRacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
