import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rules-menu',
  templateUrl: './rules-menu.component.html',
  styleUrls: ['./rules-menu.component.scss']
})
export class RulesMenuComponent implements OnInit {

  rules: any = [
    {
      title: 'Etiquette',
      text: 'Topics, triggers and character creation guidelines.',
      link: '/rules/etiquette',
    },
    {
      title: 'Home rules',
      text: 'Changes to official D&D 5e rules used in all campaigns.',
      link: '/rules/home-rules',
    },
    {
      title: 'Leveling',
      text: 'Leveling rules for Quam.',
      link: '/rules/leveling',
    },
    {
      title: 'Ship combat',
      text: 'Ship combat rules for both spelljammers and naval combat.',
      link: '/rules/ship-combat',
    },
    {
      title: 'Spelljamming',
      text: 'Spelljammer specific rules for Quam campaign.',
      link: '/rules/spelljamming',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
