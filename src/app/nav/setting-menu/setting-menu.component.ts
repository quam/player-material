import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting-menu',
  templateUrl: './setting-menu.component.html',
  styleUrls: ['./setting-menu.component.scss']
})
export class SettingMenuComponent implements OnInit {

  constructor() { }

  setting: any = [
    {
      title: 'Planar Empire',
      text: 'General setting info, for the Planar empire.',
      link: '/setting/planar-empire',
    },
    {
      title: 'Races',
      text: 'Full list of playable races.',
      link: '/setting/races',
    },
  ]

  ngOnInit(): void {
  }

}
