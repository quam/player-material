import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignsMenuComponent } from './campaigns-menu.component';

describe('CampaignsMenuComponent', () => {
  let component: CampaignsMenuComponent;
  let fixture: ComponentFixture<CampaignsMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignsMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
