import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaigns-menu',
  templateUrl: './campaigns-menu.component.html',
  styleUrls: ['./campaigns-menu.component.scss']
})
export class CampaignsMenuComponent implements OnInit {

  campaigns: any = [
    {
      title: 'Rise of lady Irulan',
      text: 'Epic tale of the matriarch of the Baley dynasty.',
      link: '/campaigns/rise-of-lady-irulan',
    },
    {
      title: 'Green Dot',
      text: 'Epic journey that reconnected the continents.',
      link: '/campaigns/green-dot',
    },
    {
      title: 'Quam',
      text: 'The stories from the southern continent.',
      link: '/campaigns/quam',
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
