import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebrewMenuComponent } from './homebrew-menu.component';

describe('HomebrewMenuComponent', () => {
  let component: HomebrewMenuComponent;
  let fixture: ComponentFixture<HomebrewMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomebrewMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebrewMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
