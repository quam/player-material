import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homebrew-menu',
  templateUrl: './homebrew-menu.component.html',
  styleUrls: ['./homebrew-menu.component.scss']
})
export class HomebrewMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  homebrew: any = [
    {
      title: 'Races',
      text: 'Homebrew races that can be used for PC creation.',
      link: '/homebrew/races',
    },
    {
      title: 'Spells',
      text: 'Additional spells available as well as PC signature spells.',
      link: '/homebrew/spells',
    },
    {
      title: 'Items',
      text: 'Items and artifacts received by the players.',
      link: '/homebrew/items',
    },
  ];
}
