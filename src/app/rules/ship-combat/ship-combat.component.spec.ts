import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipCombatComponent } from './ship-combat.component';

describe('ShipCombatComponent', () => {
  let component: ShipCombatComponent;
  let fixture: ComponentFixture<ShipCombatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShipCombatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipCombatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
