import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpelljammingComponent } from './spelljamming.component';

describe('SpelljammingComponent', () => {
  let component: SpelljammingComponent;
  let fixture: ComponentFixture<SpelljammingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpelljammingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpelljammingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
