import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRulesComponent } from './home-rules.component';

describe('HomeRulesComponent', () => {
  let component: HomeRulesComponent;
  let fixture: ComponentFixture<HomeRulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRulesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
