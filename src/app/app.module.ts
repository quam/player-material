import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { MainComponent } from './common/main/main.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { MenuComponent } from './common/menu/menu.component';
import { CampaignsMenuComponent } from './nav/campaigns-menu/campaigns-menu.component';
import { RulesMenuComponent } from './nav/rules-menu/rules-menu.component';
import { HomebrewMenuComponent } from './nav/homebrew-menu/homebrew-menu.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { RulesComponent } from './rules/rules.component';
import { HomebrewComponent } from './homebrew/homebrew.component';
import { RiseOfLadyIrulanComponent } from './campaigns/rise-of-lady-irulan/rise-of-lady-irulan.component';
import { GreenDotComponent } from './campaigns/green-dot/green-dot.component';
import { QuamComponent } from './campaigns/quam/quam.component';
import { RacesComponent } from './homebrew/races/races.component';
import { SpellsComponent } from './homebrew/spells/spells.component';
import { ItemsComponent } from './homebrew/items/items.component';
import { EtiquetteComponent } from './rules/etiquette/etiquette.component';
import { HomeRulesComponent } from './rules/home-rules/home-rules.component';
import { LevelingComponent } from './rules/leveling/leveling.component';
import { ShipCombatComponent } from './rules/ship-combat/ship-combat.component';
import { SpelljammingComponent } from './rules/spelljamming/spelljamming.component';
import { RatfolkComponent } from './homebrew/races/ratfolk/ratfolk.component';
import { SpellComponent } from './common/spell/spell.component';
import { SettingComponent } from './setting/setting.component';
import { PlanarEmpireComponent } from './setting/planar-empire/planar-empire.component';
import { SettingMenuComponent } from './nav/setting-menu/setting-menu.component';
import { PlayableRacesComponent } from './setting/playable-races/playable-races.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    MainMenuComponent,
    MenuComponent,
    CampaignsMenuComponent,
    RulesMenuComponent,
    HomebrewMenuComponent,
    CampaignsComponent,
    RulesComponent,
    HomebrewComponent,
    RiseOfLadyIrulanComponent,
    GreenDotComponent,
    QuamComponent,
    RacesComponent,
    SpellsComponent,
    ItemsComponent,
    EtiquetteComponent,
    HomeRulesComponent,
    LevelingComponent,
    ShipCombatComponent,
    SpelljammingComponent,
    RatfolkComponent,
    SpellComponent,
    SettingComponent,
    PlanarEmpireComponent,
    SettingMenuComponent,
    PlayableRacesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
