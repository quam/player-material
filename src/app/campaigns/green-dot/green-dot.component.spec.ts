import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenDotComponent } from './green-dot.component';

describe('GreenDotComponent', () => {
  let component: GreenDotComponent;
  let fixture: ComponentFixture<GreenDotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreenDotComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenDotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
