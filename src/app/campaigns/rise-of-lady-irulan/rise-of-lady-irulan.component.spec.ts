import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiseOfLadyIrulanComponent } from './rise-of-lady-irulan.component';

describe('RiseOfLadyIrulanComponent', () => {
  let component: RiseOfLadyIrulanComponent;
  let fixture: ComponentFixture<RiseOfLadyIrulanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiseOfLadyIrulanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiseOfLadyIrulanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
