import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuamComponent } from './quam.component';

describe('QuamComponent', () => {
  let component: QuamComponent;
  let fixture: ComponentFixture<QuamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
