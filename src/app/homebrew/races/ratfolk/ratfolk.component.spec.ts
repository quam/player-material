import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatfolkComponent } from './ratfolk.component';

describe('RatfolkComponent', () => {
  let component: RatfolkComponent;
  let fixture: ComponentFixture<RatfolkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatfolkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatfolkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
