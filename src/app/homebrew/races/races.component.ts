import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-races',
  templateUrl: './races.component.html',
  styleUrls: ['./races.component.scss']
})
export class RacesComponent implements OnInit {

  races: any = [
    {
      title: 'Ratfolk',
      text: 'Ratfolk race for D&D 5e.',
      link: '/homebrew/races/ratfolk',
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
