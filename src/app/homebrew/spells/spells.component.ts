import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-spells",
  templateUrl: "./spells.component.html",
  styleUrls: ["./spells.component.scss"],
})
export class SpellsComponent implements OnInit {

  ice_bolt = {
    title: "Ice Bolt",
    level: "cantrip (evocation)",
    casting: "1 action",
    range: "120 ft",
    components: "V, S",
    duration: "instantaneous",
    classes: "artificer, druid, wizard, sorcerer",
    context: "new earth",
    text: `You hurl a mote of ice at a creature or object within range.
      Make a ranged spell attack against the target. On a hit, the target
      takes 1d10 cold damage. This spell's damage increases by 1d10 when
      you reach 5th level (2d10), 11th level (3d10), and 17th level (4d10).`,
  };

  create_destroy_air = {
    title: "Create / Destroy Air",
    level: "1 (transmutation)",
    casting: "1 action",
    range: "self",
    components: "V, S, M (small flask)",
    duration: "instantaneous",
    classes: "cleric, druid, paladin, wizard",
    context: "spelljamming",
    text: `You refresh the air in ships envelop replenishing 4 man days
      of air. Alternatively you can destroy 4 man days of air in a ship's
      envelope. You can add or destroy 4 more man days of air for each level
      above the first.`,
  };

  arcans_mind_transposition = {
    title: "Arcan's Mind Transposition",
    level: "1 (divination)",
    casting: "1 action",
    range: "touch",
    components: "V, S",
    duration: "10 minutes, concentration",
    classes: "cleric",
    context: "signature spell",
    text: `You touch a willing creature allowing it to see, hear and feel
    everything you do, including visions, telepathic messages and religious
    experiences for the duration of spell. Creature must remain within 30 ft
    or the spell ends immediately. You can target an additional creature for
    every level above the first.`,
  };

  freezing_ray = {
    title: "Freezing Ray",
    level: "1 (evocation)",
    casting: "1 action",
    range: "60 ft",
    components: "V, S",
    duration: "instantaneous",
    classes: "druid, wizard, sorcerer",
    context: "new earth",
    text: `A ray of extreme cold lashes out toward a creature within range.
      Make a ranged spell attack against the target. On a hit, the target takes
      2d8 cold damage and must make a Constitution saving throw. On a failed save,
      it is also encased in ice and considered rooted the end of your next turn.
      When you cast this spell using a spell slot of 2nd level or higher, the
      damage increases by 1d8 for each slot level above 1st.`,
  };

  cold_arc = {
    title: "Cold Arc",
    level: "1 (evocation)",
    casting: "1 action",
    range: "30 ft",
    components: "V, S, M (a twig from a tree that has been broken off in blizzard)",
    duration: "concentration, up to 1 minute",
    classes: "druid, wizard, sorcerer, warlock",
    context: "new earth",
    text: `A beam of cold, crackling, blue energy lances out toward a creature within
    range, forming a sustained arc of cold lightning between you and the target. Make
    a ranged spell attack against that creature. On a hit, the target takes 1d12
    cold damage, and on each of your turns for the duration, you can use your action
    to deal 1d12 cold damage to the target automatically. The spell ends if you use your
    action to do anything else. The spell also ends if the target is ever outside
    the spell's range or if it has total cover from you. At Higher Levels. When
    you cast this spell using a spell slot of 2nd level or higher, the initial
    damage increases by 1d12 for each slot level above 1st.`,
  };

  ice_ball = {
    title: "Ice Ball",
    level: "3 (evocation)",
    casting: "1 action",
    range: "150 ft",
    components: "V, S, M (tiny piece of ice)",
    duration: "concentration, up to 1 minute",
    classes: "druid, wizard, sorcerer",
    context: "new earth",
    text: `A bright blue ball flashes from your pointing finger to a point you choose
      within range and then blossoms with a low roar into an explosion of ice.
      Each creature in a 20-foot-radius sphere centered on that point must make a
      Dexterity saving throw. A target takes 8d6 cold damage on a failed save, or
      half as much damage on a successful one. The area becomes a difficult terrain
      until the end of your next turn. When you cast this spell using a spell slot of
      4th level or higher, the damage increases by 1d6 for each slot level above 3rd.`,
  };

  ambers_scorching_verdict = {
    title: "Amber's Scorching Verdict",
    level: "5 (evocation)",
    casting: "1 action",
    range: "60 ft",
    components: "V, S, M (piece of ember)",
    duration: "concentration, up to 1 minute",
    classes: "druid",
    context: "signature spell",
    text: `Bright, white flames fill a 20 ft radius circle from a point
      you chose within range. As part of casting this spell you can chose
      any number of creatures. Those creatures passed your judgement and
      are unharmed by the fire. For the duration of the spell, whenever a
      creature not selected by you enters the area for the first time on a
      turn or starts its turn there, the creature makes a Constitution
      saving throw. A target takes 3d6 fire damage on a failed save, or half
      as much damage on a successful one. Whenever you or a creature you
      deemed worthy that you can see moves into the fire for the first time
      on a turn or starts its turn there, you can cause the fire to restore
      1d8 hit points to that creature. The fire can't heal constructs or
      undead (but they can still be unharmed). The fire can heal a number of
      times equal to 1 + twice your spellcasting ability modifier (minimum of
      3 times). For every level above 5th the damage is increased by 1d6 and
      you gain 2 additional uses of healing.`,
  };

  hermiones_snowstorm = {
    title: "Hermione's Snowstorm",
    level: "5 (evocation)",
    casting: "1 action",
    range: "60 ft",
    components: "V, S, M (piece of polar bear's fur)",
    duration: "concentration, up to 1 minute",
    classes: "wizard",
    context: "signature spell",
    text: `A 20-foot-diameter snow storm  appears in an space of your choice within
    range and lasts for the duration. Any creature that starts its turn inside the
    area must make a Strength saving throw. The creature takes 6d6 cold damage on a
    failed save and is knocked prone, or half as much damage on a successful one.
    As a bonus action, you can move the storm up to 30 feet. The area of the storm is
    considered heavily obscured. When you cast this spell using a spell slot of 6th
    level or higher, the damage increases by 1d6 for each slot level above 5th.`,
  };

  constructor() {}

  ngOnInit(): void {}
}
